/**
 * Defines all Gulp tasks necessary for building and testing the project's code.
 */

var gulp = require('gulp'),
	gutil = require('gulp-util'),

	browserify = require('gulp-browserify'),
	bytediff = require('gulp-bytediff'),
	concat = require('gulp-concat'),
	ngAnnotate = require('gulp-ng-annotate'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	path = require('path');

module.exports = function (end)
{
	var count = 0,
		paths = [
			'app/boot.js'
		];

	paths.map(function(appPath) {
		gulp.src(appPath)
			.pipe(concat('all.min.js', { newLine: ';' }))
			.pipe(browserify({
				insertGlobals: false,
				debug: false
			}))
			.pipe(sourcemaps.init())
			.pipe(ngAnnotate({ add: true }))
			.on('error', function (e) {
				gutil.log("ngAnnotate ERROR: " + e.message);
				process.exit(1);
			})
			.pipe(bytediff.start())
			.pipe(uglify({
				mangle: true
			}))
			.pipe(bytediff.stop())
			.pipe(sourcemaps.write('./'))
			.pipe(gulp.dest(path.dirname(appPath)))
			.on('end', function() {
				count++;
				if (!(count < paths.length)) {
					end();
				}
			});
	});
};