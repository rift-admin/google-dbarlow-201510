// Written by Deavon Barlow <deavonb@gmail.com>

/**
 * Compiles LESS files into distribution CSS files
 */

var gulp = require('gulp'),
	less = require('gulp-less'),
	minifyCSS = require('gulp-minify-css'),
	notify = require('gulp-notify');

module.exports = function ()
{
    return gulp.src(['app/assets/less/**/*.less'])
        .pipe(less())
		.pipe(minifyCSS())
        .pipe(gulp.dest('app/assets/css'))
        .pipe(notify("LESS compilation complete."));
};