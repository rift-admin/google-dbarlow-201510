// Written by Deavon Barlow <deavonb@gmail.com>

/**
 * Gulp task for compiling all necessary JavaScript for distribution.
 */

var gulp = require('gulp'),
	gutil = require('gulp-util'),
	notify = require('gulp-notify'),
	concat = require('gulp-concat'),

	through = require('through2');

module.exports = function (cb)
{
	// TODO: remove
    var build = require(process.cwd() + '/configs/build.json').concat;

    var next = function ()
	{
        if (build.length > 0) {
            var artifact = build.shift();

            // Concat and output resulting streams, deps first, then libs
            gulp.src(artifact.src).pipe(concat(artifact.dest.file))
                .pipe(gulp.dest(artifact.dest.path))
                .on('end', next);
        } else {
            // Tell Gulp when we're done processing
            through().pipe(notify("JavaScript Compilation Complete!"));
            cb();
        }
    };

    // Use recursion to force ordering of build
    next();
};