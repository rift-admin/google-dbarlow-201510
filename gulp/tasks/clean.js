// Written by Deavon Barlow <deavonb@gmail.com>

/**
 * Gulp task for cleaning all build artifacts. By cleaning before a new build, we ensure
 * zero conflicts.
 */

var gulp = require('gulp'),
	gutil = require('gulp-util'),
	rimraf = require('gulp-rimraf');

module.exports = function ()
{
	return gulp.src([
			'build/',
			'dist/',
			'app/**/*.js*',
			'app/assets/css/'
		])
		.pipe(rimraf());
};