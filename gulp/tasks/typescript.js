/**
 * Gulp task for compiling all necessary TypeScript into JavaScript
 */

var gulp = require('gulp'),
	ts = require('gulp-typescript');

module.exports = function ()
{
	return gulp.src('app/**/*.ts')
		.pipe(ts({
			module: 'commonjs',
			noImplicitAny: true,
			target: 'ES5'
		}))
		.pipe(gulp.dest('app/'));
};