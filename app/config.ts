// Written by Deavon Barlow <deavonb@gmail.com>

export default
{
	defaultLocations: [
		{
			niceName: 'Times Square',
			mailingAddress: 'Times Square, Theater District, New York, NY 10036, USA',
		}, {
			niceName: 'Mount Rushmore',
			mailingAddress: '13000 SD-244, Keystone, SD 57751, USA'
		}, {
			niceName: 'The White House',
			mailingAddress: '1600 Pennsylvania Ave NW, Washington, DC 20500, USA'
		}, {
			niceName: 'Golden Gate Bridge',
			mailingAddress: 'Golden Gate Bridge, San Francisco, CA 94129, USA'
		}, {
			niceName: 'Stonehenge',
			mailingAddress: 'A344, Salisbury, Wiltshire SP4 7DE, United Kingdom'
		}, {
			niceName: 'Great Wall of China',
			mailingAddress: 'Great Wall, Gulang, Wuwei, Gansu, China'
		}, {
			niceName: 'Hollywood Sign',
			mailingAddress: 'Hollywood Sign, Los Angeles, CA 90068, USA'
		}
	],
	geocodingApi: {
		apiBaseUrl: 'https://maps.googleapis.com/maps/api/geocode/',
		apiKey: 'AIzaSyCz6w_4OovH8HLWaz6xTzsihFXr8b6ADaM',
		responseType: 'json'
	},
	originLocation: {
		niceName: 'Left Field Labs office',
		mailingAddress: '510 Victoria, Venice, CA'
	}
};