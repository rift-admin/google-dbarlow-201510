// Written by Deavon Barlow <deavonb@gmail.com>

/// <reference path='../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />

'use strict';

import '../bower_components/angular/angular';
import '../bower_components/angular-resource/angular-resource.min';
import './common';
import config from './config';
import HomeController from './homeController';

/**
 * 1. Declare the app module
 */
angular.module('LeftFieldLabs.CodingExercise', [
	'ngResource',
	'LeftFieldLabs.CodingExercise.Common'
])
	// global config object
	.value('config', config)
	// Home page
	.controller('HomeController', HomeController);