// Written by Deavon Barlow <deavonb@gmail.com>

/// <reference path='../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />

import fitTextDirective from './common/fitTextDirective';
import travelDistanceService from './common/travelDistanceService';

/**
 * @ngdoc module
 * @name LeftFieldLabs.CodingExercise.Common
 * @module Common
 * @requires angular
 *
 * @description
 * The `LeftFieldLabs.CodingExercise.Common` module contains all components
 * shared between pages or features.
 */
angular.module('LeftFieldLabs.CodingExercise.Common', [])
	.directive('fitText', fitTextDirective)
	.service('travelDistanceService', travelDistanceService);