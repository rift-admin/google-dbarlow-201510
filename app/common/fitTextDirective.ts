// Written by Deavon Barlow <deavonb@gmail.com>

/// <reference path='../../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />

/**
 * @ngdoc directive
 * @name LeftFieldLabs.CodingExercise.Common.directive:fit-text
 * @module LeftFieldLabs.CodingExercise.Common
 * @restrict A
 *
 * @description
 * Scales text to fit predefined proportions as the document resizes or orientation changes.
 *
 * @example
 <example>
	<div fit-text='55,14,38'></div>
 </example>
 */
// @ngInject
export default function fitTextDirective($window: ng.IWindowService)
{
	// method which invokes the font resize operation
	function fitText(element: ng.IAugmentedJQuery)
	{
		window.requestAnimationFrame(function(timestamp)
		{
			var	attr	= element.attr('fit-text') || '',
				vals	= attr.split(',') || [],
				ratio: any		= vals[0] || 10,
				minSize: any	= vals[1] || 10,
				maxSize: any	= vals[2] || 200,
				elementWidth = element[0].getBoundingClientRect().width,
				fontSize = Math.max(Math.min(elementWidth / ratio, parseFloat(maxSize)), parseFloat(minSize));

			angular.element(element).css('fontSize', fontSize + 'px');
		});
	}

	return {
		link: function(scope: any, element: ng.IAugmentedJQuery)
		{
			// if the `fit` class isn't set on the element
			if (!element.hasClass('fit')) {
				// add it
				element.addClass('fit');
			}

			// on window resize or orientation change, fit the text
			angular.element($window).bind('resize orientationchange', function()
			{
				// if `fit-off` attribute isn't set
				if (!element.attr('fit-off')) {
					// resize the text
					fitText.call(this, element);
				}
			});

			// resize the text on initial directive link
			fitText.call(this, element);
		}
	};
};