// Written by Deavon Barlow <deavonb@gmail.com>

/// <reference path='../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />

// @ngInject
export default function Controller($scope: any, config: any, $resource: any, $q: any, travelDistanceService: any)
{
	// set standard variables
	$scope.origin = config.originLocation;
	$scope.destinations = config.defaultLocations;

	// Retrieve the geocode and GPS coordinates for the origin address
	var originGeocode = travelDistanceService.getDistanceByAddress( config.originLocation.mailingAddress );

	originGeocode.$promise.then(function(good)
	{
		// Set the GPS coordinates for the origin address
		$scope.origin.lat = originGeocode.results[0].geometry.location.lat;
		$scope.origin.lng = originGeocode.results[0].geometry.location.lng;

		// Loop through all default destinations
		Object.keys( config.defaultLocations ).forEach(function(i)
		{
			// Retrieve the geocode and GPS coordinates for the current destination address
			var destinationGeocode = travelDistanceService.getDistanceByAddress( config.defaultLocations[i].mailingAddress );

			destinationGeocode.$promise.then(function(good)
			{
				// Set the GPS coordinates for the current destination address
				$scope.destinations[i].lat = destinationGeocode.results[0].geometry.location.lat;
				$scope.destinations[i].lng = destinationGeocode.results[0].geometry.location.lng;

				// Calculate the distances in miles between origin and destination GPS coordinates
				var distance = travelDistanceService.getGpsDistance(
					originGeocode.results[0].geometry.location.lat,
					originGeocode.results[0].geometry.location.lng,
					destinationGeocode.results[0].geometry.location.lat,
					destinationGeocode.results[0].geometry.location.lng
				);

				$scope.destinations[i].distance = distance;
			},
			// Catch and log any errors in remote API call
			function(e) {
				console.log('ERROR: ' + config.defaultLocations[i].niceName, e);
			});
		});
	},
	// Catch and log any errors in remote API call
	function(e)
	{
		console.log('ERROR: ' + config.originLocation.niceName, e);
	});
}