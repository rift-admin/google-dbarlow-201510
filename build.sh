#!/usr/bin/env bash

# install necessary `node_modules` and `bower_components`
npm i
bower i

# build code
gulp release
gulp release