Left Field Labs - Coding Exercise
=======================
I hope you enjoy this! I had a great time building it.

## Install & Build

```
$ npm install
$ bower install
$ gulp release
```

*If Browserify should report an error when executing `gulp release`, simply reexecute the command.*


## Team

[![Deavon Barlow](http://gravatar.com/avatar/64528c7c9d45af008e3f9725b7f76440?s=200)](http://www.linkedin.com/in/deavon)
---
[Deavon Barlow](http://www.linkedin.com/in/deavon)


## License

MIT © [Deavon Barlow](http://www.linkedin.com/in/deavon)