//// Written by Deavon Barlow <deavonb@gmail.com>
//
///// <reference path='../../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />
//
///**
// * @ngdoc directive
// * @name LeftFieldLabs.CodingExercise.Common.directive:keep-aspect-ratio
// * @module LeftFieldLabs.CodingExercise.Common
// * @restrict A
// *
// * @description
// * Scales DOM element to maintain an aspect ratio within a defined parent element.
// *
// * @example
// <example>
//	<video keep-aspect-ratio='1.777'>...</video>
// </example>
// */
//// @ngInject
//export default function keepAspectRatioDirective($window: ng.IWindowService)
//{
//	// method which invokes the font resize operation
//	function keepAspectRatio(element)
//	{
//		window.requestAnimationFrame(function(timestamp)
//		{
//			var	attr	= element.attr('data-fit') || '',
//				vals	= attr.split(',') || [],
//				ratio	= vals[0] || 10,
//				minSize	= vals[1] || 10,
//				maxSize	= vals[2] || 200,
//				fontSize = Math.max(Math.min(element.width() / ratio, parseFloat(maxSize)), parseFloat(minSize));
//
//			element.css('font-size', fontSize);
//			$vid.css('margin-left', ($vid.width() - $(window).width()) / -2);
//		});
//	}
//
//	return {
//		link: function(scope, element)
//		{
//			// if the fit class isn't set on the element
//			if (!element.hasClass('fit')) {
//				// add it
//				element.addClass('fit');
//			}
//
//			// on window resize or orientation change, fit the text
//			angular.element($window).bind('resize orientationchange', function()
//			{
//				// if `fit-off` attribute isn't set
//				if (!element.attr('fit-off')) {
//					// resize the text
//					fitText.call(this, element);
//				}
//			});
//
//			// resize the text on initial directive link
//			fitText.call(this, element);
//		}
//	};
//}; 
//# sourceMappingURL=fitMediaToContainer.js.map