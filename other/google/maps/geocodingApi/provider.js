// Written by Deavon Barlow <deavonb@gmail.com>
/// <reference path='../../../../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />
var service_1 = require('./service');
/**
 * @ngdoc provider
 * @name Google.Maps.GeocodingApi.provider:$geocodingApiProvider
 * @module GeocodingApi
 *
 * @requires $injector
 *
 * @description
 * Allows for "config stage" configuration of the `$geocodingApi` service.
 *
 * @property config  Contains config variables to be set within `$l10n` service.
 */
var Provider = (function () {
    function Provider() {
    }
    /**
     * @ngdoc method
     * @name Google.Maps:$geocodingApiProvider#$get
     * @methodOf Google.Maps:$geocodingApiProvider
     *
     * @description
     * @returns {Object}
     */
    // @ngInject
    Provider.prototype.$get = function ($injector) {
        return new service_1["default"]($injector, this.config);
    };
    return Provider;
})();
exports["default"] = Provider;
//# sourceMappingURL=provider.js.map