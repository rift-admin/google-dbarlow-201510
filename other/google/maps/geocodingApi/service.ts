// Written by Deavon Barlow <deavonb@gmail.com>

/// <reference path='../../../../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />

/**
 * @ngdoc service
 * @name Google.Maps.GeocodingApi.service:$geocodingApi
 * @module GeocodingApi
 *
 * @requires $injector
 * @requires $http
 * @requires $q
 * @requires $window
 *
 * @description
 * The `$geocodingApi` Service maintains and fetches geocoded location data, utilising localStorage
 * for caching when available.
 *
 * @property {Object} cache
 * @property config
 * @property {string} localStorageToken  A string-based "key" used to get and set cache in the browser's localStorage.
 * @property {boolean} useLocalStorage  Run-time switch
 *
 * @property {ng.IHttpService} $http  A reference to Angular's `$http` singleton.
 * @property {ng.IQService} $q  A reference to Angular's `$q` singleton.
 * @property {ng.IWindowService} $window  A reference to Angular's `$window` singleton.
 */
export default class Service
{
	// declare Angular service injection vars
	private $http: ng.IHttpService;
	private $q: ng.IQService;
	private $window: ng.IWindowService;

	private cache: any;
	// set default config values which can be overridden
	private config: any = {
		apiBaseUrl: 'https://maps.googleapis.com/maps/api/geocode/json',
		apiKey: '',
		// key used to retrieve the location cache from localStorage
		localStorageToken: '',
		precachedLocations: []
	};
	private localStorageToken: string;
	private useLocalStorage: boolean;

	// prevent issues with Angular's injector after minification
	static $inject = ['$injector'];

	constructor(private $injector: any, config: any)
	{
		// make some registered Angular services available to this class
		this.$http = this.$injector.get('$http');
		this.$q = this.$injector.get('$q');

		// override any Service functions or properties with those set by Provider
		angular.extend(this.config, config);

		// start pre-fetching any location data as early as possible
		if (typeof this.config.precachedLocations !== 'undefined' && this.config.precachedLocations.length > 0)
		{
			// pre-fetch default locations, ignoring any current cache to save computation time
			this.getByAddress(this.config.precachedLocations, true);
		}

		// remember if localStorage is supported by browser
		this.$window = $injector.get('$window');
		this.useLocalStorage = this._isLocalStorageSupported();

		if (this.useLocalStorage) {
			this.cache = this._getCache();
		}

		// if cache is null by now, instantiate it
		if (this.cache === null)
		{
			this.cache = {
				collections: {},
				timestamps: {}
			};
		}
	}

	/**
	 * @ngdoc method
	 * @name Google.Maps.GeocodingApi.service:$geocodingApi#_isLocalStorageSupported
	 *
	 * @returns {boolean} If localStorage is supported by the current browser.
	 */
	private _isLocalStorageSupported()
	{
		try {
			return 'localStorage' in this.$window && this.$window['localStorage'] !== null;
		} catch (e) {
			return false;
		}
	}

	/**
	 * @ngdoc method
	 * @name Google.Maps.GeocodingApi.service:$geocodingApi#_getPersistentCache
	 * @description
	 * Gets the location cache from localStorage.
	 */
	private _getCache()
	{
		return angular.fromJson(
			localStorage.getItem(this.localStorageToken)
		);
	}

	/**
	 * @ngdoc method
	 * @name Google.Maps.GeocodingApi.service:$geocodingApi#_setlocalStorageFromCache
	 * @description
	 * Saves the location cache to localStorage.
	 */
	private _setlocalStorageFromCache()
	{
		return localStorage.setItem(
			this.localStorageToken,
			angular.toJson(this.cache)
		);
	}

	/**
	 * @ngdoc method
	 * @name Google.Maps.GeocodingApi.service:$geocodingApi#getLocation
	 *
	 * @description
	 * Fetches geocoded location data from Google Maps' Geocoding API.
	 *
	 * @param {Array.<string>} requestedCollectionKeys The keys for collections to return.
	 * @param {boolean} [ignoreCache=false] If true, will ignore local cache and always fetch location from Google.
	 *
	 * @example
	 <example>
	 <file name="PageController.js">
	 function PageController($l10n)
	 {
		$l10n.getByAddress('lightbox-client').then(function(collections) {
			// do something with `locationGeocode`
		});
	}
	 </file>
	 </example>
	 */
	getByAddress(requestedCollectionKeys: Array<string>, ignoreCache: boolean = false) : ng.IPromise<any>
	{
		//var missingCollectionsKeys = requestedCollectionKeys;
		//var cachedCollections = {};
		//
		//if (!ignoreCache) {
		//	// Leveraging pre-downloaded collections in getByAddress() request
		//
		//	// loop through cached collections
		//	for (var i; i < requestedCollectionKeys.length; i++) {
		//
		//		if (this._isCollectionCached( requestedCollectionKeys[i] ))
		//		{
		//			// requested collection has already been cached
		//
		//			delete missingCollectionsKeys[i];
		//			cachedCollections[ requestedCollectionKeys[i] ] = this.cache.collections[ requestedCollectionKeys[i] ];
		//		}
		//	}
		//}
		//
		// initialize a new promise
		var deferred = this.$q.defer();
		//
		//// we need to download some new collections
		//if (missingCollectionsKeys.length) {
		//
		//	// configure the remote L10n call
		//	var callUrl = Service.getServiceCallUrl(this.config.dataServiceUrl, this.config.clientId, this.config.locale, missingCollectionsKeys);
		//	var callConfig = {
		//		// cache the server calls
		//		cache: true,
		//		// transform the response via static function
		//		transformResponse: Service.transformRemoteCollectionsResponse
		//	};
		//
		//	// make the remote L10n call
		//	this.$http.jsonp(callUrl, callConfig)
		//		// when it's successful
		//		.success(remoteCollections => {
		//
		//			// cache the new remote collections for quick reuse?
		//			if (cacheRemote) {
		//				this._cacheCollections(remoteCollections);
		//			}
		//
		//			// build the full requested collections object by
		//			// joining cached collections with downloaded collections
		//			for (var key in remoteCollections) {
		//				cachedCollections[key] = remoteCollections.collections[key];
		//			}
		//
		//			// set this promise as successful, sending requested collections
		//			deferred.resolve(remoteCollections);
		//
		//			// when there's an error
		//		}).error(e => {
		//			// set this promise as having an error
		//			deferred.reject(e);
		//		}
		//	);
		//}
		//// all requested collections are already available
		//else {
		//	// set this promise as successful
		//	deferred.resolve(cachedCollections);
		//}
		//
		return deferred.promise;
	}

	/**
	 * @ngdoc method
	 * @name Google.Maps.GeocodingApi.service:$geocodingApi#_cacheCollections
	 * @function
	 * @description
	 * Sets the specified collections.
	 *   {
	 *     "collection-name":{
	 *       "strings": {}
	 *   }
	 *
	 * @param {Object.<collectionObj>} collectionsObj
	 * @returns {Servic}
	 */
	private _cacheCollections(newCollections: any)
	{
		for (var collectionKey in newCollections.collections) {
			this.cache.collections[collectionKey] = newCollections.collections[collectionKey];
			this.cache.timestamps[collectionKey] = Date.now(); // http://jsperf.com/gettime-vs-now-0
		}

		if (this.useLocalStorage) {
			this._setlocalStorageFromCache();
		}
	}
}