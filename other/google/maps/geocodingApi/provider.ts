// Written by Deavon Barlow <deavonb@gmail.com>

/// <reference path='../../../../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />
import Service from './service';

/**
 * @ngdoc provider
 * @name Google.Maps.GeocodingApi.provider:$geocodingApiProvider
 * @module GeocodingApi
 *
 * @requires $injector
 *
 * @description
 * Allows for "config stage" configuration of the `$geocodingApi` service.
 *
 * @property config  Contains config variables to be set within `$l10n` service.
 */
export default class Provider
{
	config: any;

	/**
	 * @ngdoc method
	 * @name Google.Maps:$geocodingApiProvider#$get
	 * @methodOf Google.Maps:$geocodingApiProvider
	 *
	 * @description
	 * @returns {Object}
	 */
	// @ngInject
	$get($injector: ng.auto.IInjectorService)
	{
		return new Service($injector, this.config);
	}
}