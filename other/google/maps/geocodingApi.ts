// Written by Deavon Barlow <deavonb@gmail.com>

/// <reference path='../../../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />
import Provider from './geocodingApi/provider';

/**
 * @ngdoc module
 * @name Google.Maps
 * @module GeocodingApi
 * @requires angular
 *
 * @description
 * The `Google.Maps.GeocodingApi` module encapsulates all client functions
 * for Google's Maps Geocoding API.
 */
angular.module('Google.Maps.GeocodingApi', [])
	.provider('$geocodingApi', Provider);