// Written by Deavon Barlow <deavonb@gmail.com>
/// <reference path='../../../bower_components/DefinitelyTyped/angularjs/angular.d.ts' />
var provider_1 = require('./geocodingApi/provider');
/**
 * @ngdoc module
 * @name Google.Maps
 * @module GeocodingApi
 * @requires angular
 *
 * @description
 * The `Google.Maps.GeocodingApi` module encapsulates all client functions
 * for Google's Maps Geocoding API.
 */
angular.module('Google.Maps.GeocodingApi', [])
    .provider('$geocodingApi', provider_1["default"]);
//# sourceMappingURL=geocodingApi.js.map