// Written by Deavon Barlow <deavonb@gmail.com>

/**
 * DEFAULT GULPFILE
 * Defines all Gulp tasks necessary for building and testing the project's code.
 */

var gulp = require('./gulp')({
	// Build Tasks
	'clean': [],
	'inject': [],

	// JavaScript Tasks
	'typescript': [],
	'browserify': ['typescript'],
	'concat': ['clean'],

	// CSS Tasks
	'less': ['clean']
});

// High level tasks (aliases)
gulp.task('release', ['build']);
gulp.task('build', ['less', 'browserify']);
gulp.task('default', ['clean']);